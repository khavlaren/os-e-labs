#include <cstdio>
#include "tree.h"

int main() {
    struct TreeNode *root, *i = nullptr;
    createTree(&root);
    for (int j = 0; j < 100; j++) {
        root = insertNode(root, root, 2 * j);
    }
    printf("Высота дерева: %d.\n", height(root));
    print(root, 0);
    clear(&root);
    return 0;
}