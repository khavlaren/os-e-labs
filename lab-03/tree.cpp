#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

#define MAX(a, b) (a > b ? a : b)

void createTree(struct TreeNode ** root) {
    *root = NULL;
}

struct TreeNode * createNode(int key, struct TreeNode * parent) {
    struct TreeNode * node = (struct TreeNode *)malloc(sizeof(struct TreeNode));
    if (!node) {
        printf("Не хватает памяти.");
        exit(0);
    }
    node->left = node->right = NULL;
    node->key = key;
    node->parent = parent;
    node->height = 1;
    return node;
}

int height(struct TreeNode * node) {
    return node ? node->height : 0;
}

int balanceFactor(struct TreeNode * node) {
    return height(node->right) - height(node->left);
}

void fixHeight(struct TreeNode * node) {
    int heightLeft = height(node->left), heightRight = height(node->right);
    node->height = MAX(heightLeft, heightRight) + 1;
}

struct TreeNode * rotateRight(struct TreeNode * p) {
    struct TreeNode * q = p->left;
    p->left = q->right;
    q->right = p;
    if (p->left) {
        p->left->parent = p;
    }
    q->parent = p->parent;
    p->parent = q;
    fixHeight(p);
    fixHeight(q);
    return q;
}

struct TreeNode * rotateLeft(struct TreeNode * q) {
    struct TreeNode * p = q->right;
    q->right = p->left;
    p->left = q;
    if (q->right) {
        q->right->parent = q;
    }
    p->parent = q->parent;
    q->parent = p;
    fixHeight(q);
    fixHeight(p);
    return p;
}

struct  TreeNode * balance(struct TreeNode * p) {
    fixHeight(p);
    if (balanceFactor(p) == 2) {
        if (balanceFactor(p->right) < 0) {
            p->right = rotateRight(p->right);
        }
        return rotateLeft(p);
    }
    else if (balanceFactor(p) == -2) {
        if (balanceFactor(p->left) > 0) {
            p->left = rotateLeft(p->left);
        }
        return rotateRight(p);
    }
    return p;
}

struct TreeNode * insertNode(struct TreeNode * node, struct TreeNode * parent, int key) {
    if (!node) {
        return createNode(key, parent);
    }
    if (key < node->key) {
        node->left = insertNode(node->left, node, key);
    }
    else if (key > node->key){
        node->right = insertNode(node->right, node, key);
    }
    else {
        return node;
    }
    return balance(node);
}

struct TreeNode * leftestOf(struct TreeNode * node) {
    return node->left ? leftestOf(node->left) : node;
}

struct TreeNode * removeLeftest(struct TreeNode * node) {
    if (!node->left) {
        return node->right;
    }
    struct TreeNode * temp = removeLeftest(node->left);
    if (temp) {
        temp->parent = node;
    }
    node->left = temp;
    return balance(node);
}

struct TreeNode * removeNode(struct TreeNode * node, int key) {
    if (!node) {
        return NULL;
    }
    struct TreeNode * temp = NULL;
    if (key < node->key) {
        temp = removeNode(node->left, key);
        if (temp) {
            temp->parent = node;
        }
        node->left = temp;
    }
    else if (key > node->key) {
        temp = removeNode(node->right, key);
        if (temp) {
            temp->parent = node;
        }
        node->right = temp;
    }
    else {
        struct TreeNode *left = node->left, *right = node->right;
        temp = node->parent;
        free(node);
        if (!right) {
            return left;
        }
        struct TreeNode * min = leftestOf(right);
        min->parent = temp;
        min->right = removeLeftest(right);
        if (min->right) {
            min->right->parent = min;
        }
        min->left = left;
        if (min->left) {
            min->left->parent = min;
        }
        return balance(min);
    }
    return balance(node);
}

struct TreeNode * search(struct TreeNode * root, int key) {
    while (root) {
        if (key < root->key) {
            root = root->left;
        }
        else if (key > root->key) {
            root = root->right;
        }
        else {
            return root;
        }
    }
    return NULL;
}

void print(struct TreeNode * node, int level) {
    if (!node)
        return;
    print(node->left, level + 1);
    for (int i = 0; i < level; i++) {
        printf("--|");
    }
    printf(" %d\n", node->key);
    print(node->right, level + 1);
}

void clearNode(struct TreeNode * node) {
    if (!node)
        return;
    clearNode(node->left);
    free(node);
    clearNode(node->right);
}

void clear(struct TreeNode ** node) {
    clearNode(*node);
    *node = NULL;
}

struct TreeNode * begin(struct TreeNode * root) {
    if (root) {
        while (root->left) {
            root = root->left;
        }
    }
    return root;
}

struct TreeNode * end(struct TreeNode * root) {
    if (root) {
        while (root->right) {
            root = root->right;
        }
    }
    return root;
}

struct TreeNode * successor(struct TreeNode * node) {
    struct TreeNode * next = NULL;
    if (node->right) {
        next = node->right;
        while (next->left) {
            next = next->left;
        }
        return next;
    }
    else {
        next = node->parent;
        while (next) {
            if (next->left == node) {
                break;
            }
            node = next;
            next = node->parent;
        }
        return next;
    }
}

struct TreeNode * predecessor(struct TreeNode * node) {
    struct TreeNode * previous = NULL;
    if (node->left) {
        previous = node->left;
        while (previous->right) {
            previous = previous->right;
        }
        return previous;
    }
    else {
        previous = node->parent;
        while (previous) {
            if (previous->right == node) {
                break;
            }
            node = previous;
            previous = node->parent;
        }
        return previous;
    }
}

int preOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *)) {
    if (!root) {
        return 0;
    }
    return (*func)(root) + preOrderSum(root->left, func) + preOrderSum(root->right, func);
}

int postOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *)) {
    if (!root) {
        return 0;
    }
    return postOrderSum(root->left, func) + postOrderSum(root->right, func) + (*func)(root);
}

int inOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *)) {
    if (!root) {
        return 0;
    }
    return inOrderSum(root->left, func) + (*func)(root) + inOrderSum(root->right, func);
}
