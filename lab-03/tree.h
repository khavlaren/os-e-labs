#ifndef TREE_TREE_H
#define TREE_TREE_H

struct TreeNode {

    struct TreeNode * left;
    struct TreeNode * right;
    struct TreeNode * parent;
    int height;
    int key;

};

void createTree(struct TreeNode ** root);

struct TreeNode * insertNode(struct TreeNode * node, struct TreeNode * parent, int key);

struct TreeNode * removeNode(struct TreeNode * node, int key);

void print(struct TreeNode * node, int level);

void clear(struct TreeNode ** root);

struct TreeNode * begin(struct TreeNode * root);

struct TreeNode * end(struct TreeNode * root);

struct TreeNode * successor(struct TreeNode * node);

struct TreeNode * predecessor(struct TreeNode * node);

struct TreeNode * search(struct TreeNode * root, int key);

int preOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *));

int postOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *));

int inOrderSum(struct TreeNode *root, int (*func)(struct TreeNode *));

int height(struct TreeNode * node);

#endif