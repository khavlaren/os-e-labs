#include <iostream>
#include <string>
#include <fstream>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;


int main(int argc, char *argv[])
{
    std::string prefix, filename;
    std::cout << "Enter prefix of filenames: ";
    std::cin >> prefix;
    std::cout << "Enter result filename: ";
    std::cin >> filename;
    std::ofstream out(filename, std::ios_base::app);
    std::string path = "./";
    for (const auto &p : fs::directory_iterator(path))
    {
        std::string s;
        const fs::path& filepath = p.path();
        const fs::path fileName = filepath.filename();
        std::string stringFileName = fileName.u8string();
        if (stringFileName.substr(0, prefix.length()) == prefix) {
            std::ifstream in(fileName);
            while (getline(in, s))
            {
                out << s << std::endl;
            }
        }
    }
    return 0;
}
