#include <cstdio>
#include <cstdlib>
#include <linux/msdos_fs.h>
#include <cstring>

#define FIRST_CLUSTER 2

static FAT * ft;

FAT * fat_use(char * partition ){
    struct fat_boot_sector * newSector = (struct fat_boot_sector *) partition;
    unsigned short NumberOfBytesPerSector = Converter(unsigned short, newSector -> sector_size);
    ft = ((FAT *)malloc(sizeof (FAT)));
    ft -> NumberOfBytesPerCluster = NumberOfBytesPerSector * newSector -> sec_per_clus ;
    ft -> DataAreaStart = NumberOfBytesPerSector*newSector->reserved + (newSector->fats * (newSector->fat32_length * NumberOfBytesPerSector));
    return ft;
}

const char * get_cluster(FILE f, ushort cluster_number) {
    fseek(f , (ft->DataAreaStart + (cluster_number - FIRST_CLUSTER) * ft->NumberOfBytesPerCluster), SEEK_SET);
    return readFromFile(ft->NumberOfBytesPerCluster);
}


bool fat_read(FILE * f, size_t len , void * buf) {

    if (f == NULL) {
        return false;
    }

    const char * start_cluster = get_cluster(f, f->startingCluster);
    buf = memcpy(buf, (const void*)start_cluster, len);
    free((void*)start_cluster);

    return true;
}

int main(int argc, char *argv[]) {
    char partition[] = "mbr";
    ft = fat_use(partition);
    FILE * fp;
    char filename[255];
    scanf("%s", filename);
    fp = fopen(filename, "r");
    char buff[255];
    fat_read(fp, 255, buff);
    printf("%s\n", buff);
    fclose(fp);
}

